<?php declare(strict_types=1);

use Order\OrderFacade;

$orderId = 123;
/** @var OrderFacade $orderFacade */
$orderFacade = null;

$order = $orderFacade->getOrderById($orderId);
if ($order === null) {
    throw new RuntimeException('Order not found');
}

echo json_encode([
    'id' => $order->getId(),
    'client' => [
        'id' => $order->getClient()->getId(),
        'name' => $order->getClient()->getName(),
    ],
    'author' => [
        'id' => $order->getAuthor()->getId(),
        'name' => implode(' ', [$order->getAuthor()->getLastName(), $order->getAuthor()->getFirstName(), $order->getAuthor()->getMiddleName()]),
    ],
], JSON_THROW_ON_ERROR);