<?php declare(strict_types=1);

use Service\RailService;

require_once __DIR__ . '/vendor/autoload.php';

$entityManager = EntityManagerFactory::create();

$rail = $entityManager->find(RailService::class, 16);

echo $rail->getOrder()->getId();