<?php declare(strict_types=1);

namespace PHPSTORM_META
{

    use Doctrine\Persistence\ObjectManager;

    override(ObjectManager::find(0),
        map([
            '' => '@',
        ])
    );
}