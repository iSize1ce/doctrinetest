<?php declare(strict_types=1);

use Profile\ProfileFacade;

$profileId = 123;
/** @var ProfileFacade $profileFacade */
$profileFacade = null;

$profile = $profileFacade->getProfileById($profileId);
if ($profile === null) {
    throw new RuntimeException('Profile not found');
}

$firstName = $argv[1] ?? null;
$lastName = $argv[2] ?? null;
$middleName = $argv[3] ?? null;

if ($firstName === null || $lastName === null || $middleName === null) {
    throw new RuntimeException('Not valid');
}

$profile->setFirstName((string) $firstName);
$profile->setLastName((string) $lastName);
$profile->setMiddleName((string) $middleName);

$profileFacade->save($profile);
