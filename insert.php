<?php declare(strict_types=1);

use Client\Client;
use Order\Order;
use Profile\Profile;
use Service\RailService;
use Service\Service;

require_once __DIR__ . '/vendor/autoload.php';

$entityManager = EntityManagerFactory::create();

$client = new Client('HRG Russia');

$profile1 = new Profile('Pavel', 'Leschev', 'Viktorovich', $client);
$profile2 = new Profile('FirstName', 'LastName', 'MiddleName', $client);

$order1 = new Order($client, $profile1);
$service1 = new Service($order1);
$service2 = new RailService(123, $order1);

$order2 = new Order($client, $profile2);
$service3 = new Service($order2);
$service4 = new RailService(123, $order2);

$entityManager->persist($client);
$entityManager->persist($profile1);
$entityManager->persist($profile2);
$entityManager->persist($order1);
$entityManager->persist($service1);
$entityManager->persist($service2);
$entityManager->persist($order2);
$entityManager->persist($service3);
$entityManager->persist($service4);

$entityManager->flush();
