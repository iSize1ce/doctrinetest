<?php declare(strict_types=1);

namespace Client;

use Doctrine\ORM\EntityManager;

class ClientFacade
{
    private EntityManager $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getClientById(int $clientId): ?Client
    {
        return $this->entityManager->find(Client::class, $clientId);
    }
}
