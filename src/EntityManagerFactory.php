<?php declare(strict_types=1);

use Doctrine\DBAL\Logging\EchoSQLLogger;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class EntityManagerFactory
{
    public static function create(): EntityManager
    {
        $config = Setup::createAnnotationMetadataConfiguration(
            [__DIR__],
            true,
            null,
            null,
            false
        );
        $config->setSQLLogger(new EchoSQLLogger());

        return EntityManager::create(
            [
                'dbname' => 'doctrine',
                'user' => 'root',
                'password' => 'rootDbPassword',
                'host' => 'mysql',
                'driver' => 'pdo_mysql',
            ],
            $config
        );
    }
}