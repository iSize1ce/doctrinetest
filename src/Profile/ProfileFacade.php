<?php declare(strict_types=1);

namespace Profile;

use Doctrine\ORM\EntityManager;

class ProfileFacade
{
    private EntityManager $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getProfileById(int $profileId): ?Profile
    {
        return $this->entityManager->find(Profile::class, $profileId);
    }

    public function save(Profile $profile): void
    {
        $this->entityManager->persist($profile);
        $this->entityManager->flush();
    }
}
