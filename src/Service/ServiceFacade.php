<?php declare(strict_types=1);

namespace Service;

use Doctrine\ORM\EntityManager;

class ServiceFacade
{
    private EntityManager $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getServiceById(int $serviceId): ?Service
    {
        return $this->entityManager->find(Service::class, $serviceId);
    }

    public function getRailServiceById(int $serviceId): ?RailService
    {
        return $this->entityManager->find(RailService::class, $serviceId);
    }
}
