<?php declare(strict_types=1);

namespace Service;

use Doctrine\ORM\Mapping as ORM;
use Order\Order;

/**
 * @ORM\Entity()
 */
class RailService extends Service
{
    /** @ORM\Column(type="integer") */
    private int $railApiId;

    public function __construct(int $railApiId, Order $order)
    {
        parent::__construct($order);

        $this->railApiId = $railApiId;
    }

    public function getRailApiId(): int
    {
        return $this->railApiId;
    }
}