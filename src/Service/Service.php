<?php declare(strict_types=1);

namespace Service;

use Doctrine\ORM\Mapping as ORM;
use Order\Order;

/**
 * @ORM\Entity()
 * @ORM\Table(name="service")
 * @ORM\MappedSuperclass()
 * @ORM\InheritanceType("JOINED")
 */
class Service
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="Order\Order", fetch="EXTRA_LAZY")
     */
    private Order $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }
}
