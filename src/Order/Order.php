<?php declare(strict_types=1);

namespace Order;

use Client\Client;
use Doctrine\ORM\Mapping as ORM;
use Profile\Profile;

/**
 * @ORM\Entity()
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="Client\Client", fetch="EXTRA_LAZY")
     */
    private Client $client;

    /**
     * @ORM\ManyToOne(targetEntity="Profile\Profile", fetch="EXTRA_LAZY")
     */
    private Profile $author;

    public function __construct(Client $client, Profile $author)
    {
        $this->client = $client;
        $this->author = $author;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function getAuthor(): Profile
    {
        return $this->author;
    }
}
