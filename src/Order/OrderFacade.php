<?php declare(strict_types=1);

namespace Order;

use Doctrine\ORM\EntityManager;

class OrderFacade
{
    private EntityManager $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getOrderById(int $orderId): ?Order
    {
        return $this->entityManager->find(Order::class, $orderId);
    }
}
